<?php
/**
 * The template for displaying search results.
 * 
 * @package wlasny
 */

get_header();

$main_column_size = GetMainColumnSize();
?> 
<?php get_sidebar('left'); ?> 
				<div class="col-md-<?php echo $main_column_size; ?> content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php if (have_posts()) { ?> 
						<header class="page-header">
							<h1 class="page-title"><?php printf(__('Search Results for: %s', 'wlasny'), '<span>' . get_search_query() . '</span>'); ?></h1>
						</header>
						<?php 
						while (have_posts()) {
							the_post();	
							get_template_part('content', 'search');
						}
						BasicPagination();
						?> 
						<?php } else { ?> 
						<?php get_template_part('no-results', 'search'); ?>
						<?php } ?> 
					</main>
				</div>
<?php get_sidebar('right'); ?> 
<?php get_footer(); ?> 
