<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<div class="entry-meta">
			<?php BasicPostOn(); ?> 
		</div>
	</header>

	<div class="entry-content">
		<?php the_content(); ?> 
		<div class="clearfix"></div>
		<?php
		wp_link_pages(array(
			'before' => '<div class="page-links">' . __('Strony:', 'wlasny') . ' <ul class="pagination">',
			'after'  => '</ul></div>',
			'separator' => ''
		));
		?> 
	</div>
	<footer class="entry-meta">
		<?php
			$category_list = get_the_category_list(__(', ', 'wlasny'));
			$tag_list = get_the_tag_list('', __(', ', 'wlasny'));
			echo BasicCategoriesList($category_list);
			if ($tag_list) {
				echo ' ';
				echo BasicTagsList($tag_list);
			}
			echo ' ';
			printf(__('<span class="glyphicon glyphicon-link"></span> <a href="%1$s" title="Link do %2$s" rel="bookmark">link</a>.', 'wlasny'), get_permalink(), the_title_attribute('echo=0'));
		?> 

		<?php BasicEditPostLink(); ?> 
	</footer>
</article>