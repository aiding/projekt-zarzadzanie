<?php get_header(); ?> 
				<div class="col-md-12 content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<section class="error-404 not-found">
							<header class="page-header">
								<h1 class="page-title"><?php _e('Niestety nie znaleziono strony', 'wlasny'); ?></h1>
							</header>
							<div class="page-content">
								<p><?php _e('Wygląda na to, że nie znaleziono tego, czego szukasz. Moze wyszukiwarka pomoże?', 'wlasny'); ?></p>
								<form class="form-horizontal" method="get" action="<?php echo esc_url(home_url('/')); ?>" role="form">
									<div class="form-group">
										<div class="col-xs-10">
											<input type="text" name="s" value="<?php echo esc_attr(get_search_query()); ?>" placeholder="<?php echo esc_attr_x('Szukaj &hellip;', 'placeholder', 'wlasny'); ?>" title="<?php echo esc_attr_x('Szukaj &hellip;', 'label', 'wlasny'); ?>" class="form-control" />
										</div>
										<div class="col-xs-2">
											<button type="submit" class="btn btn-default"><?php _e('Szukaj', 'wlasny'); ?></button>
										</div>
									</div>
								</form>
							</div>
						</section>
					</main>
				</div>
<?php get_footer(); ?> 