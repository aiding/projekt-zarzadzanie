<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php if ('post' == get_post_type()) { ?> 
		<div class="entry-meta">
			<?php BasicPostOn(); ?> 
		</div>
		<?php }  ?> 
	</header>
	<?php if (is_search()) {  ?> 
	<div class="entry-summary">
		<?php the_excerpt(); ?> 
		<div class="clearfix"></div>
	</div>
	<?php } else { ?> 
	<div class="entry-content">
		<?php the_content(MoreLinkText()); ?> 
		<div class="clearfix"></div>
		<?php 

		wp_link_pages(array(
			'before' => '<div class="page-links">' . __('Strony:', 'wlasny') . ' <ul class="pagination">',
			'after'  => '</ul></div>',
			'separator' => ''
		));
		?> 
	</div>
	<?php }  ?> 
	<footer class="entry-meta">
		<?php if ('post' == get_post_type()) {  ?> 
		<div class="entry-meta-category-tag">
			<?php
				
				$categories_list = get_the_category_list(__(', ', 'wlasny'));
				if (!empty($categories_list)) {
			?> 
			<span class="cat-links">
				<?php echo CategoriesList($categories_list); ?> 
			</span>
			<?php }  ?> 

			<?php
				$tags_list = get_the_tag_list('', __(', ', 'wlasny'));
				if ($tags_list) {
			?> 
			<span class="tags-links">
				<?php echo BasicTagsList($tags_list); ?> 
			</span>
			<?php }  ?> 
		</div>
		<?php }  ?> 
	</footer>
</article>