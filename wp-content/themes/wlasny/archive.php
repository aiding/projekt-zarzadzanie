<?php 

get_header(); 
$main_column_size = GetMainColumnSize();

?> 
<?php get_sidebar('left'); ?> 
				<div class="col-md-<?php echo $main_column_size; ?> content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php if (have_posts()) { ?> 
						<header class="page-header">
							<h1 class="page-title">
								<?php
								if (is_category()) :
									single_cat_title();

								elseif (is_tag()) :
									single_tag_title();

								elseif (is_author()) :
									the_post();
									printf(__('Autor: %s', 'wlasny'), '<span class="vcard">' . get_the_author() . '</span>');
									rewind_posts();

								elseif (is_day()) :
									printf(__('Dzień: %s', 'wlasny'), '<span>' . get_the_date() . '</span>');

								elseif (is_month()) :
									printf(__('Miesiąc: %s', 'wlasny'), '<span>' . get_the_date('F Y') . '</span>');

								elseif (is_year()) :
									printf(__('Rok: %s', 'wlasny'), '<span>' . get_the_date('Y') . '</span>');

								elseif (is_tax('post_format', 'post-format-aside')) :
									_e('Notatki', 'wlasny');

								elseif (is_tax('post_format', 'post-format-image')) :
									_e('Obrazek', 'wlasny');

								elseif (is_tax('post_format', 'post-format-video')) :
									_e('Wideo', 'wlasny');

								elseif (is_tax('post_format', 'post-format-quote')) :
									_e('Cytat', 'wlasny');

								elseif (is_tax('post_format', 'post-format-link')) :
									_e('Linki', 'wlasny');

								else :
									_e('Archiwa', 'wlasny');

								endif;
								?> 
							</h1>
							<?php
							$term_description = term_description();
							if (!empty($term_description)) {
								printf('<div class="taxonomy-description">%s</div>', $term_description);
							}
							?>
						</header>
						<?php 
						while (have_posts()) {
							the_post();
							get_template_part('content', get_post_format());
						} 
						?> 
						<?php BasicPagination(); ?> 

						<?php } else { ?> 

						<?php get_template_part('no-results', 'archive'); ?> 

						<?php }  ?> 
					</main>
				</div>
<?php get_sidebar('right'); ?> 
<?php get_footer(); ?> 