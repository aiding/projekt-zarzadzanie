<?php
/**
 *
 * 
 * @package wlasny
 */

get_header();

$main_column_size = GetMainColumnSize();
?> 
<?php get_sidebar('left'); ?> 
				<div class="col-md-<?php echo $main_column_size; ?> content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php 
						while (have_posts()) {
							the_post();
							get_template_part('content', get_post_format());
							echo "\n\n";
							BasicPagination();
							echo "\n\n";
						}
						?> 
					</main>
				</div>
<?php get_sidebar('right'); ?> 
<?php get_footer(); ?> 
