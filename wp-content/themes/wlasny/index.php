<?php
/**
 * 
 * 
 * @package wlasny
 */

get_header();
$main_column_size = GetMainColumnSize();
?>
<?php get_sidebar('left'); ?> 
				<div class="col-md-<?php echo $main_column_size; ?> content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php if (have_posts()) { ?> 
						<?php 
						while (have_posts()) {
							the_post();
							get_template_part('content', get_post_format());
						}
						
						BasicPagination();
						?>  
						<?php } else { ?> 
						<?php get_template_part('no-results', 'index'); ?>
						<?php }  ?> 
					</main>
				</div>
<?php get_sidebar('right'); ?> 
<?php get_footer(); ?> 