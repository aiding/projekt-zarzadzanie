<?php

/**
* Plugin Name: Dodanie tekstu stopki
*/

add_action('admin_menu', 'pozycja_menu');
function pozycja_menu () {
  add_management_page('Zmiana stopki', 'Zmiana stopki', 'manage_options', __FILE__, 'zmiana_tekstu_stopki');
}

add_action( 'wp_footer', 'funkcja_zmieniajaca_tekst' );
function funkcja_zmieniajaca_tekst() {
  $var = get_option('test_plugin_variable', 'hello world');
  ?>
  
  <div class="container">
  <footer id="site-footer" role="contentinfo">
		<div id="footer-row" class="row site-footer">
			<div class="col-md-6 footer-left">
				<?php echo $var; ?>
			</div>
			<div class="col-md-6 footer-right text-right">
				<?php dynamic_sidebar('footer-right'); ?> 
			</div>
		</div>
	</footer>
 	</div>
  <?php
}

function zmiana_tekstu_stopki () {
  $var = get_option('test_plugin_variable', 'hello world');
  if (isset($_POST['change-clicked'])) {
    update_option( 'test_plugin_variable', $_POST['footertext'] );
    $var = get_option('test_plugin_variable', 'hello world');
  }

?>

<div class="wrap">
  <h1>Treść stopki</h1>
  <p>Wpisz treść stopki:</p>
  <form action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="post">
  <input style="width: 700px;" type="text" value="<?php echo $var; ?>" name="footertext" placeholder="hello world"><br />
  <input name="change-clicked" type="hidden" value="1" />
  <br />
  <input type="submit" value="Wyślij" />
  </form>
</div>

<?

}

