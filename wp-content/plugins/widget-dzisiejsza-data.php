<?php
/*
Plugin Name: Własny widget z aktualną datą
Author: Piotr Wronowski
Version: 1
*/


class DataWidget extends WP_Widget
{
  function DataWidget()
  {
    $widget_ops = array('classname' => 'DataWidget', 'description' => 'Wyświetla dzisiejszą datę' );
    $this->WP_Widget('DataWidget', 'Widget wyświetlający dzisiejszą datę', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
	?>
  		<p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
	<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 		if(ICL_LANGUAGE_CODE=='pl'):
			echo "<p>Dzisiaj jest:</p>";
 		elseif(ICL_LANGUAGE_CODE=='en'):
			echo "<p>Today is:</p>";
		endif; 
 $pfx_date = get_the_date( $format, $post_id );
 
 echo  $pfx_date;

 
    echo $after_widget;
  }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("DataWidget");') );?>